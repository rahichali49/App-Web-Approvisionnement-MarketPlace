def buildJar() {
    sh 'cd Backend && mvn clean package -DskipTests'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'git-hub-credential', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'cd Backend && docker build -t alirahich/repo-pipeline-project:latest .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push alirahich/repo-pipeline-project:latest'
    }
}

def deployApp() {
    echo 'deploying the application...'
} 


return this
